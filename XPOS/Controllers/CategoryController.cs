﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XPOS.Repository;
using XPOS.ViewModel;

namespace XPOS.Controllers
{
    public class CategoryController : Controller
    {
        // GET: Category
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            return PartialView("_List", CategoryRepo.GetAll());
        }

        public ActionResult CategoryList(string search = "")
        {
            return PartialView("_List", CategoryRepo.GetBySearch(search));
        }

        public ActionResult Create()
        {
            return PartialView("_Create", new VariantViewModel());
        }

        [HttpPost]
        public ActionResult Create(CategoryViewModel model)
        {
            ResponseResult result = CategoryRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            },JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(int id)
        {
            return PartialView("_Edit", CategoryRepo.GetById(id));
        }

        [HttpPost]
        public ActionResult Edit(CategoryViewModel model)
        {
            ResponseResult result = CategoryRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int id)
        {  
          return PartialView("_Delete", CategoryRepo.GetById(id));
        }
        

        [HttpPost]
        public ActionResult Delete(CategoryViewModel model)
        {
            ResponseResult result = CategoryRepo.Delete(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }

    }
}