﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XPOS.Repository;
using XPOS.ViewModel;

namespace XPOS.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult List()
        {
            List<ProductViewModel> ListProduct = ProductRepo.GetAll();
            return PartialView("_List", ListProduct);
        }

        public ActionResult ListByVariant(long id = 0)
        {
            return PartialView("_ListByVariant", ProductRepo.ByVariant(id));
        }

        public ActionResult ProductList(string search = "")
        {
            return PartialView("_List", ProductRepo.GetBySearch(search));
        }

        public ActionResult Create()
        {
            ViewBag.CategoryList = new SelectList(CategoryRepo.GetAll(), "Id", "Name");
            ViewBag.VariantList = new SelectList(VariantRepo.ByCategory(0), "Id", "Name");
            return PartialView("_Create",new ProductViewModel());
        }
        [HttpPost]
        public ActionResult Create(ProductViewModel model)
        {
            ResponseResult result = ProductRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int Id)
        {
            //id => Product id
            ProductViewModel model = ProductRepo.GetById(Id);
            ViewBag.CategoryList = new SelectList(CategoryRepo.GetAll(), "id", "Name");
            ViewBag.VariantList = new SelectList(VariantRepo.ByCategory(model.CategoryId), "id", "Name");
            return PartialView("_Edit", model);

        }

        [HttpPost]
        public ActionResult Edit(ProductViewModel model)
        {
            ResponseResult result = ProductRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                massage = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Delete(int id)
        {
            ViewBag.VariantList = new SelectList(VariantRepo.GetAll(), "id", "Name");
            return PartialView("_Delete", ProductRepo.GetById(id));

        }

        [HttpPost]
        public ActionResult Delete(ProductViewModel model)
        {
            ResponseResult result = ProductRepo.Delete(model);
            return Json(new
            {
                success = result.Success,
                massage = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }
    }
}