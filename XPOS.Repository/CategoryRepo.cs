﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPOS.ViewModel;
using XPOSModel;

namespace XPOS.Repository
{
    public class CategoryRepo
    {
        //Get ALL
        public static List<CategoryViewModel> GetAll()
        {
                List<CategoryViewModel> result = new
                    List<CategoryViewModel>();
                using (var db = new XPOSContext())
                {
                    result = (from cat in db.Categories
                              select new CategoryViewModel
                              {
                                  id = cat.Id,
                                  Initial = cat.Initial,
                                  Name = cat.Name,
                                  Active = cat.Active

                              }).ToList();
                }
                return result;
        }

        public static List<CategoryViewModel> GetBySearch(string search)
        {
            List<CategoryViewModel> result = new List<CategoryViewModel>();
            using (var db = new XPOSContext())
            {
                result = db.Categories
                    .Where(o => o.Active == true && (o.Name.Contains(search)))
                    .Take(10)
                    .Select(d => new CategoryViewModel
                    {
                        id = d.Id,
                        Initial = d.Initial,
                        Name = d.Name,
                        Active = d.Active
                    }).ToList();
            }
            return result;
        }
        public static CategoryViewModel GetById(int id)
        {
            CategoryViewModel result = new CategoryViewModel();
            using (var db = new XPOSContext())
            {
                result = (from cat in db.Categories
                          where cat.Id == id
                          select new CategoryViewModel
                          {
                              id = cat.Id,
                              Initial = cat.Initial,
                              Name = cat.Name,
                              Active = cat.Active
                          }).FirstOrDefault();
            }
            return result != null ? result : new CategoryViewModel();
        }


        public static ResponseResult Update(CategoryViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPOSContext())
                {
                    #region Create New/Insert
                    if (entity.id == 0)
                    {
                        Category category = new Category();
                        category.Initial = entity.Initial;
                        category.Name = entity.Name;
                        category.Active = entity.Active;
                        category.CreatedBy = "Zaipin";
                        category.CreatedDate = DateTime.Now;

                        db.Categories.Add(category);
                        db.SaveChanges();

                        result.Entity = entity;

                    }
                    else
                    {
                        Category category = db.Categories.Where(o => o.Id == entity.id).FirstOrDefault();
                        if (category != null)
                        {
                            category.Initial = entity.Initial;
                            category.Name = entity.Name;
                            category.Active = entity.Active;
                            category.ModifiedBy = "Monica";
                            category.ModifiedDate = DateTime.Now;

                            db.SaveChanges();

                            result.Entity = entity;

                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "Category not found";
                        }

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }



        public static ResponseResult Delete(CategoryViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPOSContext())
                {
                    Category category = db.Categories.Where(o => o.Id == entity.id).FirstOrDefault();
                    if (category != null)
                    {
                        db.Categories.Remove(category);
                        db.SaveChanges();
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Category is not found";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }

            return result;
        }
    }
}