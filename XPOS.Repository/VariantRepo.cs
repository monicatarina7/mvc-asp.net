﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPOS.ViewModel;
using XPOSModel;

namespace XPOS.Repository
{
   public class VariantRepo
    {
        //Get ALL
        public static List<VariantViewModel> GetAll()
        {
            List<VariantViewModel> result = new
                List<VariantViewModel>();
            using (var db = new XPOSContext())
            {
                result = (from var in db.Variants
                          select new VariantViewModel
                          {
                              id = var.Id,
                              CategoryId = var.CategoryId,
                              Initial = var.Initial,
                              Name = var.Name,
                              Active = var.Active

                          }).ToList();
            }
            return result;
        }

        public static List<VariantViewModel> GetBySearch(string search)
        {
            List<VariantViewModel> result = new List<VariantViewModel>();
            using (var db = new XPOSContext())
            {
                result = db.Variants
                    .Where(o => o.Active == true && o.Name.Contains(search))
                    .Take(10)
                    .Select(v => new VariantViewModel
                    {
                        id = v.Id,
                        CategoryId = v.CategoryId,
                        Initial = v.Initial,
                        Name = v.Name,
                        Active = v.Active
                    }).ToList();
            }
            return result;
        }

        public static VariantViewModel GetById(int id)
        {
            VariantViewModel result = new VariantViewModel();
            using (var db = new XPOSContext())
            {
                result = (from var in db.Variants
                          where var.Id == id
                          select new VariantViewModel
                          {
                              id = var.Id,
                              Initial = var.Initial,
                              CategoryId = var.CategoryId,
                              Name = var.Name,
                              Active = var.Active
                          }).FirstOrDefault();
            }
            return result != null ? result : new VariantViewModel();
        }

        public static ResponseResult Update(VariantViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPOSContext())
                {
                    #region Create New/Insert
                    if (entity.id == 0)
                    {
                        Variant variant = new Variant();
                        variant.Initial = entity.Initial;
                        variant.CategoryId = entity.CategoryId;
                        variant.Name = entity.Name;
                        variant.Active = entity.Active;
                        variant.CreatedBy = "Zaipin";
                        variant.CreatedDate = DateTime.Now;

                        db.Variants.Add(variant);
                        db.SaveChanges();

                        result.Entity = entity;

                    }
                    else
                    {
                        Variant variant = db.Variants.Where(o => o.Id == entity.id).FirstOrDefault();
                        if (variant != null)
                        {
                            variant.Initial = entity.Initial;
                            variant.Name = entity.Name;
                            variant.CategoryId = entity.CategoryId;
                            variant.Active = entity.Active;
                            variant.ModifiedBy = "Monica";
                            variant.ModifiedDate = DateTime.Now;

                            db.SaveChanges();

                            result.Entity = entity;

                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "Category not found";
                        }

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }



        public static ResponseResult Delete(VariantViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPOSContext())
                {
                    Variant variant = db.Variants.Where(o => o.Id == entity.id).FirstOrDefault();
                    if (variant != null)
                    {
                        db.Variants.Remove(variant);
                        db.SaveChanges();
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Category is not found";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }

            return result;
        }

        public static List<VariantViewModel> ByCategory(long id)
        {
            //id=> Category id
            List<VariantViewModel> result = new List<VariantViewModel>();
            using (var db = new XPOSContext())
            {
                result = db.Variants
                    .Where(v => v.CategoryId == (id == -1 ? v.CategoryId : id))
                    .Select(c => new VariantViewModel
                    {
                        id = c.Id,
                        CategoryId = c.CategoryId,
                        Initial = c.Initial,
                        Name = c.Name,
                        Active = c.Active
                    }).ToList();
            }
            return result;
        }
        
    }
}
