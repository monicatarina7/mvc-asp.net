﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPOS.ViewModel;
using XPOSModel;

namespace XPOS.Repository
{
    public class ProductRepo
    {
        //Get All
        public static List<ProductViewModel> GetAll()
        {
            List<ProductViewModel> result = new
                List<ProductViewModel>();

            using (var db = new XPOSContext())

                return ByVariant(-1);
        }


        public static List<ProductViewModel> ByVariant(long id)
        {
            // id => Variant Id
            List<ProductViewModel> result = new List<ProductViewModel>();
            using (var db = new XPOSContext())
            {
                result = db.Products
                    .Where(p => p.VariantId == (id == -1 ? p.VariantId : id))
                    .Select(v => new ProductViewModel
                    {
                        Id = v.Id,
                        VariantId = v.VariantId,
                        VariantName = v.Variant.Name,
                        Initial = v.Initial,
                        Name = v.Name,
                        Description = v.Description,
                        Price = v.Price,
                        Active = v.Active
                    }).ToList();
            }
            return result;
        }

        //Get by Id
        public static ProductViewModel GetById(int id)
        {
            ProductViewModel result = new ProductViewModel();

            using (var db = new XPOSContext())
            {
                result = (from pro in db.Products
                          where pro.Id == id
                          select new ProductViewModel
                          {
                              Id = pro.Id,
                              VariantId = pro.Variant.Id,
                              VariantName = pro.Variant.Name,
                              Initial = pro.Initial,
                              Name = pro.Name,
                              Description = pro.Description,
                              Price = pro.Price,
                              Active = pro.Active
                          }).FirstOrDefault();
            }
            return result != null ? result : new ProductViewModel();
        }

        //Get by Search
        public static List<ProductViewModel> GetBySearch(string search)
        {
            List<ProductViewModel> result = new List<ProductViewModel>();
            using (var db = new XPOSContext())
            {
                result = db.Products
                    .Where(o => o.Active == true && o.Name.Contains(search))
                    .Take(10)
                    .Select(p => new ProductViewModel
                    {
                        Id = p.Id,
                        VariantId = p.VariantId,
                        VariantName = p.Variant.Name,
                        Initial = p.Initial,
                        Name = p.Name,
                        Description = p.Description,
                        Price = p.Price,
                        Active = p.Active
                    }).ToList();
            }
            return result;
        }


        //Update
        public static ResponseResult Update(ProductViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPOSContext())
                {
                    #region Create New / Insert 
                    if (entity.Id == 0)
                    {
                        Product product = new Product();

                        product.Initial = entity.Initial;
                        product.VariantId = entity.VariantId;

                        product.Name = entity.Name;
                        product.Description = entity.Description;
                        product.Price = entity.Price;
                        product.Active = entity.Active;

                        product.CreatedBy = "Momon";
                        product.CreatedDate = DateTime.Now;

                        db.Products.Add(product);
                        db.SaveChanges();

                        result.Entity = entity;
                    }
                    #endregion
                    #region Edit

                    else
                    {
                        Product product = db.Products.Where(o =>
                        o.Id == entity.Id).FirstOrDefault();

                        if (product != null)
                        {

                            product.VariantId = entity.VariantId;
                            product.Initial = entity.Initial;
                            product.Name = entity.Name;
                            product.Description = entity.Description;
                            product.Price = entity.Price;
                            product.Active = entity.Active;

                            product.ModifiedBy = "Momon";
                            product.ModifyDate = DateTime.Now;

                            db.SaveChanges();

                            result.Entity = entity;
                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "Product is not found";
                        }
                        #endregion
                    }
                }

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;

            }
            return result;
        }

        //Delete
        public static ResponseResult Delete(ProductViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPOSContext())
                {
                    Product product = db.Products.Where(o => o.Id == entity.Id).FirstOrDefault();
                    if (product != null)
                    {
                        result.Entity = entity;
                        db.Products.Remove(product);
                        db.SaveChanges();
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Product is not found";
                    }

                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
                throw;
            }
            return result;

        }

    }
}
